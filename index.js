// EX 1
const salary = 100000;
function showInformation() {
  // Let input value which user entered (a number)
  var input = document.getElementById("inputValue").value;
  // Input value that user input
  //   var output = input.value;

  // Processing
  var result = 0;
  result = input * salary;

  // show the result on the screen
  var spanResult = document.getElementById("showResult");
  //   spanResult.style.backgroundColor = 'green'
  spanResult.className = "bg-success p-2 ml-5 text-white";
  spanResult.innerHTML = result.toLocaleString();
}

// EX 2
function averageRealNumber() {
  // Let input value which user entered (number 1-> 5 )
  var input1 = parseFloat(document.getElementById("inputnumber1").value);
  var input2 = parseFloat(document.getElementById("inputnumber2").value);
  var input3 = parseFloat(document.getElementById("inputnumber3").value);
  var input4 = parseFloat(document.getElementById("inputnumber4").value);
  var input5 = parseFloat(document.getElementById("inputnumber5").value);

  // Processing
  var averageTotal = 0;
  averageTotal = (input1 + input2 + input3 + input4 + input5) / 5;

  // show the result on the screen
  document.getElementById("showResult2").innerHTML = averageTotal;
}

// EX 3
var btnShow = document.getElementById("clickShow3");
btnShow.onclick = function () {
  // Let input value which user entered (USD money)
  var letValue = parseInt(document.getElementById("inputValue3").value);

  // Processing
  var convert = 0;
  convert = letValue * 23500;

  // show the result on the screen
  var usdMoney = document.getElementById("showResult3");
  usdMoney.className = "bg-success p-2 ml-5 text-white";

  usdMoney.innerHTML = convert.toLocaleString() + " VND";
};

// EX 4
var btnShow4 = document.getElementById("clickShow4");
btnShow4.onclick = function () {
  // Let input value which user entered (width and height)
  var width = parseInt(document.getElementById("inputWidth").value);
  var height = parseInt(document.getElementById("inputHeight").value);

  // Processing
  var area = 0;
  var perimeter = 0;

  perimeter = (height + width) * 2;
  area = width * height;

  // show the result on the screen
  var showArea = document.getElementById("Area");
  showArea.className = "bg-success p-2 ml-5 text-white";

  var showPerimeter = document.getElementById("Perimeter");
  showPerimeter.className = "bg-success p-2 ml-5 text-white";

  showArea.innerHTML = area;
  showPerimeter.innerHTML = perimeter;
};

// EX 5
var btnShow5 = document.getElementById("clickShow5");
btnShow5.onclick = function () {
  // Let input value which user entered (2-digit integer)
  var number = parseInt(document.getElementById("inputValue5").value);

  // Processing
  var units = 0;
  var dozens = 0;
  var total = 0;

  units = number % 10;
  dozens = Math.floor(number / 10) % 10;
  // 2 way
  // dozens = Math.trunc(number / 10);
  total = parseInt(dozens) + parseInt(units);

  // show the result on the screen
  var result = document.getElementById("showResult5");
  result.className = "btn-success p-2 ml-5 text-white";
  result.innerHTML = total;
};
